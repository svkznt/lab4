package com.jdbc;

import com.mysql.cj.protocol.Resultset;

import java.sql.*;

public class Controller
{
    private DBConnection connection;
    private int countOfProducts = 0;
    Statement statement;

    public Controller(int countOfProducts, DBConnection connection) throws SQLException
    {
        this.connection = connection;
        try
        {
            String cleanTable = "truncate table business.products";
            this.statement = connection.getConnection().createStatement();
            try
            {
                statement.executeUpdate(cleanTable);
            }
            catch (Exception e)
            {
                statement.executeUpdate(createTable("business.products"));
            }

            for (int i = 0; i < countOfProducts; ++i)
            {
                add("товар" + i, (int) (Math.random() * 10000));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (statement != null)
            {
                statement.close();
            }
        }
    }

    private String createTable(String tableName)
    {
        String createTable = "create table " + tableName +
                "(id int(6) unsigned auto_increment primary key, " +
                "product_id int(6) not null, " +
                "product_name varchar(45) not null, " +
                "price double(20, 2) unsigned not null)";
        return createTable;
    }

    public void add(String name, double price) throws SQLException
    {
        String trySelectExisting = "select product_name from business.products where product_name=?";
        try
        {
            PreparedStatement prepTrySelect = connection.getConnection().prepareStatement(trySelectExisting);
            prepTrySelect.setString(1, name);;
            ResultSet resultSet = prepTrySelect.executeQuery();
            if (resultSet.next())
            {
                System.out.println("There is product with this name");
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String insert = "insert into business.products (product_id, product_name, price) values (?, ?, ?)";
        try
        {
            ++countOfProducts;
            PreparedStatement prepInsert = connection.getConnection().prepareStatement(insert);
            prepInsert.setInt(1, countOfProducts);
            prepInsert.setString(2, name);
            prepInsert.setDouble(3, price);
            prepInsert.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void delete(String name) throws SQLException
    {
        String delete = "delete from business.products where product_name=?";
        try
        {
            PreparedStatement prepDelete = connection.getConnection().prepareStatement(delete);
            prepDelete.setString(1, name);
            prepDelete.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void showAll() throws SQLException
    {
        String show = "select * from business.products";
        try
        {
            PreparedStatement prepShow = connection.getConnection().prepareStatement(show);
            ResultSet resultSet = prepShow.executeQuery();
            output(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void getPrice(String name) throws SQLException
    {
        String getPrice = "select price from business.products where product_name=?";
        try
        {
            PreparedStatement prepGetPrice = connection.getConnection().prepareStatement(getPrice);
            prepGetPrice.setString(1, name);
            ResultSet resultSet = prepGetPrice.executeQuery();
            if (resultSet.next())
            {
                double price = resultSet.getDouble("price");
                System.out.println(price);
            }
            else
            {
                System.out.println("No product with this name");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void changePrice(String name, double newPrice) throws SQLException
    {
        String changePrice = "update business.products set price=? where product_name=?";
        try
        {
            PreparedStatement prepChangePrice = connection.getConnection().prepareStatement(changePrice);
            prepChangePrice.setDouble(1, newPrice);
            prepChangePrice.setString(2, name);
            prepChangePrice.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void showInterval(double from, double to) throws SQLException
    {
        String showInterval = "select * from business.products where price between ? AND ?";
        try
        {
            PreparedStatement prepShowInterval = connection.getConnection().prepareStatement(showInterval);
            prepShowInterval.setDouble(1, from);
            prepShowInterval.setDouble(2, to);
            ResultSet resultSet = prepShowInterval.executeQuery();
            output(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void output(ResultSet resultSet) throws SQLException
    {
        while (resultSet.next())
        {
            System.out.println("id: " + resultSet.getInt("id") +
                    " " + "product_id: " + resultSet.getInt("product_id") + " " + "name: " +
                    " " + resultSet.getString("product_name") +
                    " " + "price: " + resultSet.getDouble("price"));
        }
    }
}
