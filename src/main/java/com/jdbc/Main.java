package com.jdbc;

import java.sql.*;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args) throws SQLException
    {
        DBConnection dbConnection = new DBConnection().getInstance();

        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество продуктов в таблице: ");
        int n;
        try
        {
            n = in.nextInt();
            if (n < 0)
            {
                System.out.println("Введите положительное число");
                n = in.nextInt();
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
            System.out.println("Неправильный ввод");
            return;
        }

        try
        {
            Controller controller = new Controller(n, dbConnection);
            while (true)
            {
                System.out.println("Enter the command");
                String command = in.next();
                try
                {
                    switch (command)
                    {
                        case "/add":
                        {
                            String name = in.next();
                            double price = in.nextDouble();
                            if (price < 0)
                            {
                                System.out.println("Price must be > 0");
                                break;
                            }
                            controller.add(name, price);
                            break;
                        }
                        case "/delete":
                        {
                            String name = in.next();
                            controller.delete(name);
                            break;
                        }
                        case "/show_all":
                        {
                            controller.showAll();
                            break;
                        }
                        case "/price":
                        {
                            String name = in.next();
                            controller.getPrice(name);
                            break;
                        }
                        case "/change_price":
                        {
                            String name = in.next();
                            double newPrice = in.nextDouble();
                            if (newPrice < 0)
                            {
                                System.out.println("Price must be > 0");
                                break;
                            }
                            controller.changePrice(name, newPrice);
                            break;
                        }
                        case "/filter_by_price":
                        {
                            double from = in.nextDouble();
                            double to = in.nextDouble();
                            if (from < 0 || to < 0)
                            {
                                System.out.println("Enter positive numbers");
                                break;
                            }

                            if (from > to)
                            {
                                System.out.println("Incorrect interval");
                                break;
                            }
                            controller.showInterval(from, to);
                            break;
                        }
                        case "/end":
                        {
                            break;
                        }
                        default:
                            System.out.println("Wrong command, try again");
                            break;
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (dbConnection != null)
            {
                dbConnection.getConnection().close();
            }
        }
    }
}
